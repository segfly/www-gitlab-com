/* eslint-disable no-new, no-undef, eqeqeq, no-eq-null */

(function() {
  var GITLAB_EEP_SUBSCRIPTION_PRICE = 199;

  var paramsFetcher = function() {
    var regex = /[?&]([^=#]+)=([^&#]*)/g;
    var params = {};
    var match = '';

    // eslint-disable-next-line no-cond-assign
    while (match = regex.exec(window.location.href)) {
      params[match[1]] = match[2];
    }

    return (Object.keys(params).length > 0) ? params : null;
  };

  var formatAmount = function(amount) {
    return '$' + amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  var validateNumbersInput = function() {
    var num = this.value.match(/^\d+$/);
    if (num === null) this.value = '';
  };

  var paramsParser = function(params) {
    if (!params) return null;
    var validParams = {
      teamInfo: {},
      features: {}
    };

    Object.keys(params).forEach(function(param) {
      if (param === 'team_size' || param === 'developer_cost') {
        validParams.teamInfo[param] = params[param];
      } else if (params[param] ===  '0') {
        validParams.features[param] = params[param];
      }
    });

    return validParams;
  };

  var checkFeaturesInCategory = function(categoryKey, toCheck) {
    var $features = $('input[data-parent-category-key=' + categoryKey + ']');
    $features.each(function(index, element) {
      element.checked = toCheck;
    });
  };

  this.RoiCalculator  = (function() {
    function RoiCalculator(params) {
      this.$featuresTable = $('.features-table table tr');
      this.$featuresTableInputs = $('.include input', this.$featuresTable);
      this.$teamSizeInput = $('.js-developer-team-size');
      this.$costDeveloperInput = $('.js-cost-per-developer');
      this.$savingsText = $('.savings-text h3');
      this.$copyLinkButton = document.querySelector('.js-copy-link');
      this.$totalSubscription = $('.total-subscription');
      this.$toggleCategories = $('input[data-category-key]');
      this.params = params;
      this.bindEvents.call(this);

      if (this.params) {
        this.fillValues.call(this);
        this.checkCategories(this.$toggleCategories);
      }
      this.calculateSavings.call(this);
    }

    RoiCalculator.prototype.bindEvents = function() {
      this.$featuresTableInputs.on('click', this.calculateSavings.bind(this));
      this.$featuresTableInputs.on('click', this.checkCategory.bind(this));
      this.$toggleCategories.on('click', this.toggleCategory.bind(this));
      this.$teamSizeInput.on('keyup input', validateNumbersInput);
      this.$teamSizeInput.on('keyup input', this.calculateSavings.bind(this));
      this.$costDeveloperInput.on('keyup input', validateNumbersInput);
      this.$costDeveloperInput.on('keyup input', this.calculateSavings.bind(this));
    };

    RoiCalculator.prototype.checkCategory = function(e) {
      var toggledFeature = e.target;
      var categoryKey = toggledFeature.dataset.parentCategoryKey;
      var $category = $('input[data-category-key=' + categoryKey + ']');

      this.checkCategories($category);
    };

    RoiCalculator.prototype.checkCategories = function(toggleCategories) {
      toggleCategories.each(function(index, toggleCategoryElement) {
        var $features = $('input[data-parent-category-key=' + toggleCategoryElement.dataset.categoryKey + ']');
        toggleCategoryElement.checked = true;

        $features.each(function(i, el) {
          if (!el.checked) {
            toggleCategoryElement.checked = false;
          }
        });
      });
    };

    RoiCalculator.prototype.toggleCategory = function(e) {
      checkFeaturesInCategory(e.target.dataset.categoryKey, e.target.checked);
      this.calculateSavings();
    };

    RoiCalculator.prototype.calculateSavings = function() {
      var totalSavings = 0;
      var costPerDeveloper = parseFloat(this.$costDeveloperInput.val() || 0);
      var teamSize = parseFloat(this.$teamSizeInput.val() || 0);
      this.$featuresTable.each(function() {
        var currentFeatureCheckbox = $('.include input', this);
        var currentFeatureSavingsPerUser = $('.savings-per-user', this);
        var currentFeatureSavingsTotal = $('.savings-total', this);
        if (currentFeatureCheckbox.is(':checked')) {
          var incidents = parseFloat(currentFeatureCheckbox.data('incidents-year'));
          var hours = parseFloat(currentFeatureCheckbox.data('hours-incident'));
          var result = incidents * hours * costPerDeveloper;
          currentFeatureSavingsPerUser.text(formatAmount(result.toFixed()));
          currentFeatureSavingsTotal.text(formatAmount((result * teamSize).toFixed()));
          totalSavings = totalSavings += result;
        }
      });
      totalSavings = totalSavings *= teamSize;
      this.$savingsText.text(formatAmount(totalSavings.toFixed()) + ' per year saved');
      this.$totalSubscription.text('With a $199 per year per user GitLab Enterprise Edition Premium subscription ' +
                                  '(' + formatAmount(GITLAB_EEP_SUBSCRIPTION_PRICE * teamSize) + ' total cost)');
      this.createUrl.call(this);
    };

    RoiCalculator.prototype.createUrl = function() {
      var params = '?';
      this.$featuresTable.each(function() {
        var currentFeatureCheckbox = $('.include input', this);
        if (!currentFeatureCheckbox.is(':checked') && currentFeatureCheckbox.length !== 0) {
          params = params += currentFeatureCheckbox.data('link-shorthand') + '=0&';
        }
      });
      params = params += 'team_size=' + (this.$teamSizeInput.val() || 0) + '&';
      params = params += 'developer_cost=' + (this.$costDeveloperInput.val() || 0);
      this.$copyLinkButton.dataset.clipboardText = this.rootUrl() + params;
      this.updateUrlBrowser(params);
      new Clipboard('.js-copy-link');
    };

    RoiCalculator.prototype.fillValues = function() {
      Object.keys(this.params.features).forEach(function(param) {
        this.$featuresTableInputs.filter('[data-link-shorthand="' + param + '"]').prop('checked', false);
      }.bind(this));
      this.$costDeveloperInput.val(this.params.teamInfo.developer_cost);
      this.$teamSizeInput.val(this.params.teamInfo.team_size);
    };

    RoiCalculator.prototype.rootUrl = function() {
      var url = window.location.href;
      if (url.indexOf('?') >= 0) {
        url = url.split('?')[0];
      }
      return url;
    };

    RoiCalculator.prototype.updateUrlBrowser = function(urlParams) {
      var stateObject = { roi_state: 'updated' };
      if (history.state != null) {
        history.replaceState(stateObject, '', urlParams);
      } else {
        history.pushState(stateObject, '', urlParams);
      }
    };

    return RoiCalculator;
  })();

  // Add the params once we get the basic functionality going
  var urlParams = paramsFetcher();
  var parsedParams = paramsParser(urlParams);

  if (parsedParams) {
    new RoiCalculator(parsedParams);
  } else {
    new RoiCalculator(null);
  }
})();
