---
layout: handbook-page-toc
title: "Scalability Team"
---

![Scalability Team logo: inspired by the album cover of Unknown Pleasures, the debut studio album by English rock band Joy Division, except the waveforms are Tanukis.](img/scalability_team_logo.png)

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common Links

| **Issue Trackers** | https://gitlab.com/gitlab-com/gl-infra/scalability |
| **Slack Channels** | [#g_scalability](https://gitlab.slack.com/archives/g_scalability) (Primary Team Channel), [#infrastructure-lounge](https://gitlab.slack.com/archives/infrastructure-lounge) (Infrastructure Group Channel), [#incident-management](https://gitlab.slack.com/archives/incident-management) (Incident Management),  [#alerts-general](https://gitlab.slack.com/archives/alerts-general) (SLO alerting), [#mech_symp_alerts](https://gitlab.slack.com/archives/mech_symp_alerts) (Mechanical Sympathy Alerts) |

## Mission

The **Scalability team** is responsible for GitLab and GitLab.com at scale,
working on the highest priority scalability items in the application in close
coordination with **Reliability Engineering** teams and providing feedback
to other Engineering teams so they can become better at scalability as well.

## Vision

As its name implies, the Scalability team enhances the **availability**,
**reliability** and, **performance** of GitLab by observing applications
capabilities to operate at GitLab.com scale.
The **Scalability team** analizes application performance on GitLab.com,
recognizes bottlenecks in service availability, proposes short term improvements
and develops long term plans that help drive the decisions of other Engineering teams.

Short term goals include:

- Refine existing, define new ones and document the process for [Service Level Objectives](https://en.wikipedia.org/wiki/Service-level_objective)
for each of GitLab services.
- Continuously expose top 3 critical bottlenecks that threaten the stability of
GitLab.com.
- Work on scoping, planning and defining the implementation steps of the top critical
bottleneck.
- Define and track team KPI's to track impact on GitLab.com and GitLab as an
application.

## Work prioritization process

All work tracked by the team is compiled in the [Scaling GitLab.com epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/148).

When we need to work in the [GitLab.org group](https://gitlab.com/groups/gitlab-org), we create a corresponding epic there and link it in the above epic's description (as epics are tied to groups, and we use more than one top-level group).

Diagram below describes how the work gets prioritized in the Scalability team, and added to the above mentioned epic:

![workflow](img/workflow.png)

Process contains 6 cyclical stages:

1. **Observe** - What is causing to SLA and SLO degradations on GitLab.com?
1. **Analysis** - Why is availability being reduced, do we have all information,
and are our metrics sufficient?
1. **Proposed Improvements** - Issue with a (partial, temporary or full, permanent)
fix is created including proposals for estimated SLA improvements for services affected.
1. **Triage** - Prioritise changes based on pre-defined set of rules, which
include ownership of the change.
1. **Development & Deployment** - The work on developing and ensuring that the
change has no unexpected effects is executed by the owner defined in the previous stage.
1. **Assessment** - Assesment of the implemented change is done through retrospecting
on the expected and observed state. The retrospective process is documented in
an issue that is marked related with the original issue driving the change.

## Team work processes

### Labels

The Scalability team routinely uses the following set of labels:

1. The team label, `team::Scalability`.
1. Priority labels.
1. Scoped `workflow` labels.
1. Scoped `Service` labels.

The `team::Scalability` label is used in order to allow for easier filtering of
issues applicable to the team that have group level labels applied.

The priority labels allow us to track the issues correctly and raise/lower priority
of work based on the both external and internal factors. Priorities are set
based on [the priority definitions](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels)
with an addition that the target SLO's apply to GitLab.com service SLO's.

This means that if resolving an issue will immediately improve, or is
unblocking an issue that will immediately impact GitLab.com SLO's
issue should have the highest priority.

#### Workflow labels

The Scalability team leverages scoped workflow labels to track different stages of work.
They show the progression of work for each issue and allow us to remove blockers or change
focus more easily.

The standard progression of workflow is described below:

```mermaid
sequenceDiagram
    workflow|Triage ->> workflow|Proposal: 1
Note right of workflow|Triage: Problem has been<br/>scoped and issue has<br/>a proposal ready for<br/> review.
    workflow|Proposal ->> workflow|Ready: 2
Note right of workflow|Proposal: Proposal has no <br/> blockers and <br/> work can start.    
    workflow|Ready ->> workflow|In Progress: 3
Note right of workflow|Ready: Issue is assigned and<br/> work has started.
    workflow|In Progress ->> workflow|Under Review: 4
Note right of workflow|In Progress: Issue has a MR in<br/> review.
    workflow|Under Review ->> workflow|Verify: 5
Note right of workflow|Under Review: MR was merged<br/>issue is completing<br/>set of verification <br/>steps.
    workflow|Verify ->> workflow|Done: 6
Note right of workflow|Verify: Issue is updated with<br/>the latest graphs<br/> and measurements,<br/> workflow|Done label<br/> is applied and issue<br/> can be closed.
```

There are three other workflow labels of importance omitted from the diagram above:

1. `workflow::Cancelled`:
  - Work in the issue is being abandoned due to external factors or decision to not resolve the issue. After applying this label, issue will be closed.
1. `workflow::Stalled`
  - Work is not abandoned but other work has higher priority. After applying this label, team Engineering Manager is mentioned in the issue to either change the priority or find more help.
1. `workflow::Blocked`
  - Work is blocked due external dependencies or other external factors. After applying this label, issue will be regularly triaged by the team until the label can be removed.

### Issues

Issue is being implemented if:

1. Issue has a team member assigned to it.
1. Assigned issue has a priority label set.
1. Issue has "~workflow::In Progress" set.

Issue is resolved when:

1. The problem defined in the issue has been addressed.
1. Issue description is updated with a graph comparing before/after state (if applicable).
1. Issue has "~workflow::Done" set.

### Issue boards

The Scalability team [issue boards](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/) track
the progress of ongoing work. Purpose of some of the more important issue boards
are described below:

1. [Workflow board](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/1290868)
  - Tracks the whole team ongoing workload.
1. [Abandoned work board](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/1428754)
  - Tracks the work that is not progressing.
1. Individual services board, for example [Sidekiq board](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/1428695)
  - Tracks the workload for the individual service.
1. [Priority board](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/1428893)
  - Tracks the workload based on issue priorities.

### Choosing something to work on

We work from our main epic: [Scaling GitLab on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/148).

Most of our work happens on the current in-progress sub epic. This is always prominently visible from the main epic's description. From there, work takes place on the board associated to the current in-progress epic.

Priority and workflow labels take precedence; we don't use issue ordering in boards or epics for priorities. Workflow labels to the right are higher priority than those to the left.

## Team counterparts

The **Scalability team** will work with all engineering teams across all departments
as a representative of GitLab.com as one of the largest GitLab installations,
to ensure that GitLab continues to scale in a safe and sustainable way.

[The Memory team](/handbook/engineering/development/enablement/memory/) is a natural
counterpart to the Scalability team, but their missions are complementing each other
rather than overlap:

- The Scalability team is focused on GitLab.com first, self-managed only when necessary.
  - The Memory team is focused on resolving application bottlenecks for all types of GitLab installations.
- The Scalability team is driven by set SLO objectives, regardeless of the nature of the issue.
  - The Memory team is focused on application performance and resource consumption, in all environments.
- The Scalability team primary concern is preventing disruptions of GitLab.com SLO objectives through
changes in the application architecture.
  - The Memory team primary concern is managing the application performance for all types of GitLab installations.

Simply put:

- The Scalability team is focused on all work that affects GitLab.com SLOs.
- The Memory team is focused on general GitLab resource consumption and performance.

### Team Members

The following people are members of the Scalability Team:

<%= direct_team(manager_role: 'Engineering Manager, Scalability')%>

# How do I engage with the Scalability Team?

1. Start with an issue in the Scalability team tracker: [Create an issue](https://gitlab.com/gitlab-com/gl-infra/scalability/issues/new).
1. You are welcome to follow this up with a Slack message in [#g_scalability](https://gitlab.slack.com/archives/g_scalability).
1. Please don't add any `workflow` labels to the issue. The team will triage the issue and apply these.
1. We use our [Workflow board](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/1290868) to track the workflow of issues.

# Celebrating our wins

We celebrate our wins!  Whenever a change driven by the Scalability Team shows a clear positive impact on the scalability of GitLab.com;
through key metrics, saturation reduction, reduced Mean time to Detection (MTTD), improved [Mean time between Failures](/handbook/business-ops/metrics/#mean-time-between-failures-mtbf), etc, we post a message as a comment on this snippet in our tracker:
[https://gitlab.com/gitlab-com/gl-infra/scalability/snippets/1900609](https://gitlab.com/gitlab-com/gl-infra/scalability/snippets/1900609).
